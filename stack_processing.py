"""stack_processing.py: Analysis of a z stack image.TIF."""

__author__ = "Benjamin GALLOIS"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Benjamin GALLOIS"
__email__ = "benjamin.gallois@curie.fr"
__status__ = "Production"




import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import skimage
from skimage import io
from skimage.io.collection import ImageCollection,MultiImage
import glob
from skimage import filter as filters







def stack_importation(chemin):
# Import a stack of images .TIF in a 3D matrix
	img = MultiImage(chemin)
	size = len(img)
	shape = img[0].shape
	stack = np.zeros((shape[0],shape[1],size))
	for i in range(size):
		stack[:,:,i] = img[i]
	return stack, size, shape


def MIP(matrix,shape):
# Maximum intensity projection of a 3D matrix
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			projection[x, y] = max(matrix[x, y, :])
	return projection


def number_MIP(matrix,shape,number):
# N-maximums intensity projection of a 3D matrix
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			array_sort = np.sort(matrix[x, y, :])
			projection[x, y] = sum(array_sort[-number::])
	return projection



def otsufilter(img):
# Otsu filter, return a mask where embryo regions are set to 1, and background regions set to 0
	val = filters.threshold_otsu(img)
	mask = img < val 
	mask = np.invert(mask)
	return mask



def mean_intensity_total(matrix,shape):
# Mean intensity of an image divided by the number of pixels (surface), exclude all the black pixels (I=0)
	area = 0
	for j in range(shape[0]):
		for k in range(shape[1]):
			if matrix[j,k] != 0:
				area = area + 1.
	return np.sum(matrix)/area



def main(chemin):
# main function, open a stack of images, and compute the mean intensity excluding the background.
	(img, size, shape) = stack_importation(chemin)
	projection_corrected = number_MIP(img,shape,10)
	mask = otsufilter(projection_corrected)
	mean = mean_intensity_total(mask*projection_corrected,shape)
	return mean


def label_diff(i,j,text,X,Y):
# add a P-value on a barchart
    x = (X[i]+X[j])/2
    y = 1.1*max(Y[i], Y[j])
    dx = abs(X[i]-X[j])
    props = {'connectionstyle':'bar','arrowstyle':'-',\
                 'shrinkA':20,'shrinkB':20,'lw':2}
    ax.annotate(text, xy=(X[i]+width,y+0.8), zorder=1)
    ax.annotate('', xy=(X[i],y), xytext=(X[j],y), arrowprops=props)









####''' Opening and processing stacks, return lists of mean intensity, one list per condition'''####
#######################################################################################################
chemin_StbmMO = glob.glob('./Fichiers_de_test/*StbmMO_*491.TIF')
mean_StbmMO= []
for i in range(len(chemin_StbmMO)):
	mean_StbmMO.append((main(chemin_StbmMO[i])*50)/(80.))

chemin_rescue10 = glob.glob('./Fichiers_de_test/*StbmMO+*491.TIF')
mean_rescue10 = []
for i in range(len(chemin_rescue10)):
	mean_rescue10.append((main(chemin_rescue10[i])*50)/(80.))


chemin_Ctrl = glob.glob('./Fichiers_de_test/*CtrlMO_*491.TIF')
mean_Ctrl = []
for i in range(len(chemin_Ctrl)):
	mean_Ctrl.append((main(chemin_Ctrl[i])))

chemin_Ctrl_rescue = glob.glob('./Fichiers_de_test/*CtrlMO+*491.TIF')
mean_rescue = []
for i in range(len(chemin_Ctrl_rescue)):
	mean_rescue.append((main(chemin_Ctrl_rescue[i])*50)/(80.))

#######################################################################################################









##################'''Barchart plotting'''########################
#######################################################################################################
N = 4
men_means = (np.mean(mean_Ctrl)/np.mean(mean_Ctrl),np.mean(mean_StbmMO)/np.mean(mean_Ctrl),np.mean(mean_rescue10)/np.mean(mean_Ctrl),np.mean(mean_rescue)/np.mean(mean_Ctrl))
men_std = (np.std(mean_Ctrl)/np.mean(mean_Ctrl),np.std(mean_StbmMO)/np.mean(mean_Ctrl),np.std(mean_rescue10)/np.mean(mean_Ctrl),np.std(mean_rescue)/np.mean(mean_Ctrl))

ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars
stat = np.round(sc.stats.mannwhitneyu(mean_StbmMO,mean_Ctrl),5)
stat = 'p = ' + str(stat[1])
print stat
stat1 = np.round(sc.stats.mannwhitneyu(mean_rescue10,mean_Ctrl),5)
print stat1
stat1 = 'p = ' + str(stat1[1])
stat2 = np.round(sc.stats.mannwhitneyu(mean_rescue,mean_Ctrl),5)
print stat2
stat2 = 'p = ' + str(stat2[1])
fig, ax = plt.subplots()
rects1 = ax.bar(ind, men_means, width, color='r', yerr=men_std)
label_diff(0,1,stat,ind+width*.5,men_means)
label_diff(0,2,stat1,ind+width*.5,men_means)
label_diff(0,3,stat2,ind+width*.5,men_means)
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('CrtlMO (N = '+str(len(mean_Ctrl))+ ')','StbmMO (N = '+str(len(mean_StbmMO)) + ')' , 'StbmMO rescue (N = '+str(len(mean_rescue10)) + ')' , 'CtrlMO rescue (N = '+str(len(mean_rescue)) + ')'))
plt.ylim(ymax=2.5)
plt.show()
#######################################################################################################