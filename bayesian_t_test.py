import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import pandas as pd
import pymc as pm  
from pymc.Matplot import plot as mcplot
import matplotlib.mlab as mlab


def data_analysis(path):
	(mean_StbmMO, mean_rescue10, mean_Ctrl, mean_rescue) = np.loadtxt(path, skiprows=1, usecols=(0,1,2,3), unpack=True)
	data = [mean_StbmMO, mean_rescue10, mean_Ctrl, mean_rescue]
	return data

def group(group1,group2):
	return np.concatenate((group1,group2))


def model(pooled):
	mu1 = pm.Normal("mu_1",mu=pooled.mean(), tau=1.0/pooled.var()/1000.0)  
	mu2 = pm.Normal("mu_2",mu=pooled.mean(), tau=1.0/pooled.var()/1000.0)
	sig1 = pm.Uniform("sigma_1",lower=pooled.var()/1000.0,upper=pooled.var()*1000)  
	sig2 = pm.Uniform("sigma_2",lower=pooled.var()/1000.0,upper=pooled.var()*1000)
	v = pm.Exponential("nu",beta=1.0/29)
	t1 = pm.NoncentralT("t_1",mu=mu1, lam=1.0/sig1, nu=v, value=group1[:], observed=True)  
	t2 = pm.NoncentralT("t_2",mu=mu2, lam=1.0/sig2, nu=v, value=group2[:], observed=True)
	model = pm.Model( [t1, mu1, sig1, t2, mu2, sig2, v] )
	mcmc = pm.MCMC(model)
	mcmc.sample(40000,10000,2) 
	#mcplot(mcmc) 
	mus1 = mcmc.trace('mu_1')[:]  
	mus2 = mcmc.trace('mu_2')[:]  
	sigmas1 = mcmc.trace('sigma_1')[:]  
	sigmas2 = mcmc.trace('sigma_2')[:]  
	nus = mcmc.trace('nu')[:] 
	diff_mus = mus1-mus2  
	diff_sigmas = sigmas1-sigmas2  
	normality = np.log(nus)  
	effect_size = (mus1-mus2)/np.sqrt((sigmas1**2+sigmas2**2)/2.)
	plt.figure()
	n, bins, patches = plt.hist(diff_mus, 50, normed=1, facecolor='green', alpha=0.75)
	p = (diff_mus > 0).mean()
	print p
	return p 

plt.figure()
group1 = np.random.normal(15,5,1000)  
group2 = np.random.normal(20,5,1000)
n, bins, patches = plt.hist(group1, 50, normed=1, facecolor='green', alpha=0.75)
n, bins, patches = plt.hist(group2, 50, normed=1, facecolor='red', alpha=0.75)
pooled = np.concatenate((group1,group2)) 
print 'baye : ' , model(pooled)
print 'W : ',sc.stats.mannwhitneyu(group1,group2)[1]
plt.show()