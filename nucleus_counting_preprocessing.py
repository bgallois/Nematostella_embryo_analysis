"""nucleus_counting_preprocessing.py: """

__author__ = "Benjamin GALLOIS"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Benjamin GALLOIS"
__email__ = "benjamin.gallois@curie.fr"
__status__ = "Production"





import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import skimage
from skimage.morphology import watershed
from skimage import morphology
from skimage import io, color
from skimage import filter as filters
from skimage import segmentation
from skimage.feature import peak_local_max
from scipy import ndimage
from skimage.measure import regionprops, label



'''Functions
'''

# apply an otsu filter and return a 2 bit image
def otsufilter():
	nucleus = color.rgb2gray(io.imread('dna.jpeg')) # import DAPI signal
	val = filters.threshold_otsu(nucleus)
	mask = nucleus < val # make a binary image
	mask = np.invert(mask)
	return nucleus , mask



def nucleus_counting(original , mask , size):
	distance = ndimage.distance_transform_edt(mask)
	local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)), labels=mask)
	markers, num_features = ndimage.label(local_maxi)
	labels = watershed(-distance, markers, mask=mask)
	markers[~mask] = -1
	labels_rw = segmentation.random_walker(mask, markers)
	regions = regionprops(labels)
	regions = [r for r in regions if r.area > size] # count the number of nucleus with a minimal area to define
	regions_rw = regionprops(labels_rw)
	regions_rw = [r for r in regions_rw if r.area > size]

	center = []
	center_rw = []
	for i in range(len(regions)):
		center.append(regions[i].centroid)
	for j in range(len(regions_rw)):
		center_rw.append(regions_rw[j].centroid)

	x,y = zip(*center)
	k,l = zip(*center_rw)
	
	plt.figure()
	plt.subplot(224)
	io.imshow(labels, cmap='gray', interpolation='nearest')
	plt.scatter(y,x)
	plt.title('Watershed')
	plt.subplot(223)
	io.imshow(labels_rw, cmap='gray', interpolation='nearest')
	plt.scatter(l,k)
	plt.title('Random walkers')
	plt.subplot(221)
	io.imshow(original, cmap='gray', interpolation='nearest')
	plt.title('Original')
	plt.subplot(222)
	io.imshow(mask, cmap='gray', interpolation='nearest')
	plt.title('After Otsu')

	return len(regions),len(regions_rw)


# main
original, otsu = otsufilter()
count1 , count2 = nucleus_counting(original , otsu , 100.)
print count1
print count2
print (count1+count2)/2.


plt.show()
