'''
Author : Benjamin GALLOIS
Description : This programm count the number of nucleus in a Z-projet image of a DAPI signal of a confocal stack.
'''





import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import skimage
from skimage.morphology import watershed
from skimage import morphology
from skimage import io, color
from skimage import filter as filters
from skimage import segmentation
from skimage.feature import peak_local_max
from scipy import ndimage
from skimage.measure import regionprops, label






def random_circles(number):
	'''nucleus = color.rgb2gray(io.imread('dna.jpeg')) # import DAPI signal
	val = filters.threshold_otsu(nucleus)
	mask = nucleus < val # make a binary image
	mask = np.invert(mask)'''
	for i in range(number):
		x1 = np.random.random_integers(10,190)
		y1 = np.random.random_integers(10,190)
		x, y = np.indices((200, 200))
		r1 = 7
		mask_circle1 = (x - x1) ** 2 + (y - y1) ** 2 < r1 ** 2
		if i ==0:
			mask = mask_circle1
		if i != 0:
			mask = mask + mask_circle1 
	return mask



def nucleus_counting(mask):
	distance = ndimage.distance_transform_edt(mask)
	local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)), labels=mask)
	markers, num_features = ndimage.label(local_maxi)
	labels = watershed(-distance, markers, mask=mask)
	markers[~mask] = -1
	labels_rw = segmentation.random_walker(mask, markers)
	regions = regionprops(labels)
	regions = [r for r in regions if r.area > 10] # count the number of nucleus with a minimal area to define
	regions_rw = regionprops(labels_rw)
	regions_rw = [r for r in regions_rw if r.area > 10]

	'''
	plt.figure()
	plt.subplot(224)
	io.imshow(labels, cmap='gray', interpolation='nearest')
	plt.title('Wathermash')
	plt.subplot(223)
	io.imshow(labels_rw, cmap='gray', interpolation='nearest')
	plt.title('Random walkers')
	plt.subplot(221)
	io.imshow(mask, cmap='gray', interpolation='nearest')
	plt.title('Original')
	plt.subplot(222)
	io.imshow(distance, cmap='gray', interpolation='nearest')
	plt.title('Dist')
	print('Number of cells:', len(regions) , len(regions_rw))
	'''
	

	return len(regions),len(regions_rw)


RW = []
stdRW = []
R= []
stdR = []
for i in range(1,300):
	rw = []
	r = []
	j = 0
	while j < 3:
		r.append(nucleus_counting(random_circles(i))[0])
		rw.append(nucleus_counting(random_circles(i))[1])
		j = j + 1
	R.append(np.mean(r))
	RW.append(np.mean(rw))
	stdR.append(np.std(r))
	stdRW.append(np.std(rw))
	

fig, (ax1,ax2) = plt.subplots(2)
ax1.errorbar(range(1,300), RW, yerr=stdRW)
ax1.errorbar(range(1,300), R, yerr=stdR)
ax1.set_xlabel('Number of circles')
ax1.set_ylabel('Count')


legend = []
for i in range(len(R)):
	R[i] = ((R[i] - i - 1)/(i+1))*100
	RW[i] = ((RW[i] - i - 1)/(i+1))*100
	legend.append(str(i+1))


ind = np.arange(len(R))
width = 0.35
rects1 = ax2.bar(ind, R, width , color='b')
rects2 = ax2.bar(ind+width, RW, width,color='g')
ax2.set_ylabel('Difference (percent)')
ax2.set_xticks(ind + width / 2)
ax2.set_xticklabels(legend,rotation=90)
ax2.legend((rects1[0], rects2[0]), ('R', 'RW'))
plt.show()