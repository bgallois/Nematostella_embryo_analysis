__author__ = "Benjamin GALLOIS"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Benjamin GALLOIS"
__email__ = "benjamin.gallois@curie.fr"
__status__ = "Production"




import Tkinter as Tkinter
import tkFileDialog, Tkconstants 
import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import skimage
from skimage import io
from skimage.io.collection import ImageCollection,MultiImage
import glob
from skimage import filter as filters
import pandas as pd



####''' GUI interface, one window with 3 parameters : folder where the files are placed, the choice to display the p-value and the individual points in the bar chart'''####
#######################################################################################################
def directory():
    global rep
    dirtext ='Select directory'
    rep = tkFileDialog.askdirectory(parent=fen1, initialdir='*/', title=dirtext) 
    btnchoixrep["text"] = str(rep) if rep else dirtext

def open_file():
	global plot_file
	filetext ='Select a file'
	plot_file = tkFileDialog.askopenfilename(parent=fen1, initialdir='*/', title=filetext) 
	file["text"] = str(plot_file) if plot_file else dirtext	

def get():
	global num_max
	global num_cond
	global with_points
	global with_pvalue
	global save_data
	global label
	global pattern
	global normal
	global gain
	num_max = s.get()
	num_max = int(num_max)
	normal = int(norma.get())
	num_cond = int(s1.get())
	with_points = var.get()
	with_pvalue = var1.get()
	pattern = pat.get()
	pattern = pattern.split(',')
	gain = gai.get()
	gain = gain.split(',')
	label = lab.get()
	label = label.split(',')
	save_data = var2.get()
	btnquitter = Tkinter.Button(fen1, text='Run the analysis', command = fen1.destroy)
	btnquitter.pack()

def help():
	fen2 = Tkinter.Toplevel(fen1)
	fen2.title('Help')
	fen2.geometry('600x600')
	text = Tkinter.Label(fen2, text='First select the directory where the files are placed.\n Second choose the number of maximas that the program will project for the maximum intensity projection.\n Third select if you want the bar chart with the individual points inside the bar \n and if you want the p-value displayed on top of the bars. \n Contact : benjamin.gallois@curie.fr')
	text.pack()
	btnquitter = Tkinter.Button(fen2, text='Back', command = fen2.destroy)
	btnquitter.pack()




fen1 = Tkinter.Tk()
fen1.title('Nematostella Analysis')
fen1.geometry('600x600')
rep =''
plot_file =''
button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}
tex2 = Tkinter.Label(fen1, text='Processing of a stack of images and plotting of the result : ')
tex2.pack()
btnchoixrep = Tkinter.Button(fen1, text='Select directory', command = directory)
btnchoixrep.pack(**button_opt)
tex1 = Tkinter.Label(fen1, text='Number of maximas to project : ')
tex1.pack()
s = Tkinter.Spinbox(fen1, from_=1, to=15)
s.pack()
tex4 = Tkinter.Label(fen1, text='Number of conditions to plot : ')
tex4.pack()
s1 = Tkinter.Spinbox(fen1, from_=2, to=10)
s1.pack()
tex5 = Tkinter.Label(fen1, text='Patterns in name file (insert the N patterns separed with ,) : ')
tex5.pack()
pat = Tkinter.Entry(fen1)
pat.pack(**button_opt)
pat.insert(0, "/*CtrlMO_*491.TIF,/*StbmMO_*491.TIF,/*CtrlMO+*491.TIF,/*StbmMO+*491.TIF")
tex7 = Tkinter.Label(fen1, text='Gain : ')
tex7.pack()
gai = Tkinter.Entry(fen1)
gai.pack(**button_opt)
gai.insert(0, "80,50,50,50")
tex7 = Tkinter.Label(fen1, text='Normalized by (insert the number) : ')
tex7.pack()
norma = Tkinter.Spinbox(fen1, from_=2, to=10)
norma.pack()
tex6 = Tkinter.Label(fen1, text='Labels : ')
tex6.pack()
lab = Tkinter.Entry(fen1)
lab.pack(**button_opt)
lab.insert(0, "CtrlMO,StbmMO,CtrlMO rescue,StbmMO rescue")
tex3 = Tkinter.Label(fen1, text='Plotting of data already process in a .txt file : ')
tex3.pack()
file = Tkinter.Button(fen1, text='Select a file', command = open_file)
file.pack(**button_opt)
tex4 = Tkinter.Label(fen1, text='Options available for the plot')
tex4.pack()
var = Tkinter.IntVar()
check = Tkinter.Checkbutton(fen1, text="with points", variable=var)
check.pack()
var1 = Tkinter.IntVar()
check1 = Tkinter.Checkbutton(fen1, text="with p-value", variable=var1)
check1.pack()
var2 = Tkinter.IntVar()
check2 = Tkinter.Checkbutton(fen1, text="save datas", variable=var2)
check2.pack()
app = Tkinter.Button(fen1, text='Apply', command = get)
app.pack()
help = Tkinter.Button(fen1, text='?', command = help)
help.pack()

fen1.mainloop()

#############################################################################################################################################################################################






def stack_importation(chemin):
# This function imports a stack of images .TIF in a 3D matrix, it takes in arguments the path of the folder.
	img = MultiImage(chemin)
	size = len(img)
	shape = img[0].shape
	stack = np.zeros((shape[0],shape[1],size))
	for i in range(size):
		stack[:,:,i] = img[i]
	return stack, size, shape


def MIP(matrix,shape):
# This function makes a maximum intensity projection of a 3D matrix, it takes in arguments the 3D matrix and the shape of the matrix in a tuple.
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			projection[x, y] = max(matrix[x, y, :])
	return projection


def number_MIP(matrix,shape,number):
# This function makes a N-maximums intensity projection of a 3D matrix, it takes in arguments the 3D matrix, the shape of the 3D matri in a tuple and the number of maximas to project.
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			array_sort = np.sort(matrix[x, y, :])
			projection[x, y] = sum(array_sort[-number::])
	return projection



def otsufilter(img):
# This function applies an Otsu filter, it takes in arguments a matrix and return a mask where the background is set to 0 and the foreground to 1.
	val = filters.threshold_otsu(img)
	mask = img < val 
	mask = np.invert(mask)
	return mask



def mean_intensity_total(matrix,shape):
# This function computes the mean intensity of a matrix divided by the number of pixels (surface), excluding all the pixels set to 0. It takes in argument the matrix and the shape of the matrix in a tuple.
	area = 0
	for j in range(shape[0]):
		for k in range(shape[1]):
			if matrix[j,k] != 0:
				area = area + 1.
	return np.sum(matrix)/area



def main(chemin):
# It's the main function of the program, it takes the path to a folder where the stacks .TIFF are placed, it projects the N-maximums, sets the background to 0 and computes the mean intensity normalized by the numbers of pixels non sets to 0.
# It takes in argument the path of a folder and return the mean intensity.
	(img, size, shape) = stack_importation(chemin)
	projection_corrected = number_MIP(img,shape,num_max)
	mask = otsufilter(projection_corrected)
	mean = mean_intensity_total(mask*projection_corrected,shape)
	return mean




def saving_data(plot_labels,data):
# This function save the datas in a text file. It takes the path where to save the .txt and the data in a list of array.
	a = max(plot_labels[0],plot_labels[1],plot_labels[2],plot_labels[3])
	for i in range(len(data)):
		while len(data[i]) < a :
			data[i].append(0)
	f = pd.DataFrame({plot_labels[0] : data[0], plot_labels[1] : data[1], plot_labels[2] : data[2],plot_labels[3] : data[3]})
	f.to_csv('data.txt', sep = '\t')

def data_analysis(path):
	(mean_StbmMO, mean_rescue10, mean_Ctrl, mean_rescue) = np.loadtxt(path, skiprows=1, usecols=(0,1,2,3), unpack=True)
	data = [mean_StbmMO, mean_rescue10, mean_Ctrl, mean_rescue]
	for i in range(len(data)):
		for j in data[i]:
			if j == 0:
				data[i].remove(0)
	return (mean_StbmMO, mean_rescue10, mean_Ctrl, mean_rescue, data)


##################'''Barchart plotting'''########################
#######################################################################################################
def plotting(N, data, label, norma):
	means = []
	std = []
	plot_labels = []
	for i in range(N):
		means.append((np.mean(data[i]))/(np.mean(data[norma])))
		std.append((np.std(data[i]))/(np.mean(data[norma])))
		plot_labels.append(label[i] + '(N = ' + str(len(data[i])) + ')')

	ind = np.arange(N)  # the x locations for the groups
	width = 0.2     # the width of the bars
	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, means, width, color='y', yerr=std)
	ax.set_xticks(ind + width / 2)
	ax.set_xticklabels(plot_labels)
	plt.ylim(ymax=2)

	if with_pvalue == 1:
		def label_diff(i,j,text,X,Y):
		# This function draws a line between two bars of the bar chart and displays a text on top of it.
		# It takes in arguments the i-th and the j-th bar of the bar chart, the text to display, the center of the bars as an array and the y value of the bars as an array.
		    x = (X[i]+X[j])/2
		    y = 1.1*max(Y[i], Y[j])
		    dx = abs(X[i]-X[j])
		    props = {'arrowstyle':'|-|',\
		                 'lw':2}
		    ax.annotate('', xy=(X[i],y+0.2*(i+j)), xytext=(X[j],y+0.2*(i+j)), arrowprops=props)
		    ax.annotate(text, xy=(X[i]+dx/2,y+0.2*(i+j)+0.05), zorder=10)

		for i in range(N):
			for j in range(N):
				if j > 0 and i < j :
					st = np.round(sc.stats.mannwhitneyu(data[i],data[j]),6)
					stat = ('p = ' + str(st[1]))
					label_diff(i,j,stat,ind+width*.5,means)
		

	if with_points == 1:
		for i in range(N):
			plt.plot(np.linspace(i+0.01,i+width-0.01,len(data[i])),data[i]/(np.mean(data[norma])),'ob')

	title = 'MIP_of_' + str(num_max)
	plt.title(title)
	plt.ylim(ymax=2.5)
	if save_data == 1:
		saving_data(plot_labels,data)
		fig.savefig(title + '.png')
	


#######################################################################################################



####''' Opening and processing stacks, return lists of mean intensity, one list per condition'''####
#######################################################################################################
if rep != '':
	'''	
	chemin_StbmMO = glob.glob(rep +'/*StbmMO_*491.TIF')
	mean_StbmMO= []
	for i in range(len(chemin_StbmMO)):
		mean_StbmMO.append((main(chemin_StbmMO[i])*50)/(80.))

	chemin_rescue10 = glob.glob(rep +'/*StbmMO+*491.TIF')
	mean_rescue10 = []
	for i in range(len(chemin_rescue10)):
		mean_rescue10.append((main(chemin_rescue10[i])*50)/(80.))


	chemin_Ctrl = glob.glob(rep +'/*CtrlMO_*491.TIF')
	mean_Ctrl = []
	for i in range(len(chemin_Ctrl)):
		mean_Ctrl.append((main(chemin_Ctrl[i])))

	chemin_Ctrl_rescue = glob.glob(rep +'/*CtrlMO+*491.TIF')
	mean_rescue = []
	for i in range(len(chemin_Ctrl_rescue)):
		mean_rescue.append((main(chemin_Ctrl_rescue[i])*50)/(80.))

	data = [mean_Ctrl, mean_StbmMO, mean_rescue, mean_rescue10]
	label = ['CtrlMO', 'StbmMO', 'CtrlMO rescue', 'StbmMO rescue']
	plotting(4, data, label, 0)
	'''
	data = []
	for j in range(num_cond):
		chemin = (glob.glob(rep + pattern[j]))
		mean = []
		for i in range(len(chemin)):
			mean.append((main(chemin[i])*float(gain[j]))/float(gain[0]))
		data.append(mean)
	plotting(num_cond, data, label, normal - 1)


if plot_file != '':
	data = data_analysis(plot_file)
	save_data = 0
	label = ['CtrlMO', 'StbmMO', 'CtrlMO rescue', 'StbmMO rescue']
	plotting(4, data, label, 0)
#######################################################################################################










plt.show()



