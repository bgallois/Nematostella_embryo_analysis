__author__ = "Benjamin GALLOIS"
__license__ = "GPL"
__version__ = "2.0.0"
__maintainer__ = "Benjamin GALLOIS"
__email__ = "benjamin.gallois@curie.fr"
__status__ = "Production"





import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import skimage
from skimage import io
from skimage.io.collection import ImageCollection,MultiImage
import glob
from skimage import filters as filters





def stack_importation(chemin):
	'''This function imports a stack of images .TIF in a 3D matrix, it takes in arguments the path of the folder.'''
	img = MultiImage(chemin)
	size = len(img)
	shape = img[0].shape
	print shape
	stack = np.zeros((shape[0],shape[1],size))
	for i in range(size):
		stack[:,:,i] = img[i]
	return stack, size, shape


def MIP(matrix,shape):
	'''This function makes a maximum intensity projection of a 3D matrix, it takes in arguments the 3D matrix and the shape of the matrix in a tuple.'''
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			projection[x, y] = max(matrix[x, y, :])
	return projection


def number_MIP(matrix,shape,number):
	'''This function makes a N-maximums intensity projection of a 3D matrix, it takes in arguments the 3D matrix, the shape of the 3D matri in a tuple and the number of maximas to project.'''
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			array_sort = np.sort(matrix[x, y, :])
			projection[x, y] = np.sum(array_sort[-number::])
	return projection



def otsufilter(img):
	'''This function applies an Otsu filter, it takes in arguments a matrix and return a mask where the background is set to 0 and the foreground to 1. Change nbins with the type of images (8bits = 256, 16bits = 65536'''
        val = filters.threshold_otsu(img, nbins = 65536)
	mask = img < val 
	mask = np.invert(mask)
        mask = sc.ndimage.binary_fill_holes(mask)
	return mask



def mean_intensity_total(matrix,shape):
	'''This function computes the mean intensity of a matrix divided by the number of pixels (surface), excluding all the pixels set to 0. It takes in argument the matrix and the shape of the matrix in a tuple.'''
	area = np.count_nonzero(matrix)
	'''for j in range(shape[0]):
		for k in range(shape[1]):
			if matrix[j,k] != 0:
				area = area + 1.'''
	return np.sum(matrix)/area



def main(chemin,num_max):
	'''It's the main function of the program, it takes the path to a folder where the stacks .TIFF are placed, it projects the N-maximums, sets the background to 0 and computes the mean intensity normalized by the numbers of pixels non sets to 0.
It takes in argument the path of a folder and return the mean intensity.'''
	(img, size, shape) = stack_importation(chemin)
	projection_corrected = number_MIP(img,shape,num_max)
	mask = otsufilter(projection_corrected)
	mean = mean_intensity_total(mask*projection_corrected,shape)
	return mean




def saving_data(save_dir,label,data):
	'''This function save the datas in a text file. It takes the path where to save the .txt and the data in a list of array.'''
	for i in range(len(data)):
		file = open(str(save_dir) + '/' + label[i] + '.txt','w')
		for item in data[i]:
			file.write(str(item) +'\n') 





def data_analysis(plot_file, pattern, num_cond):
	'''This function takes a way to a folder, a list of patterns for the names and a number of conditions.
It opens the .txt files which the name matches with the pattern, stores it values in a list and append this list in the list which is returning by the function.
 Return a list (of lenght number of conditions) of lists (of variable lenght). '''
	data = []
	for j in range(num_cond):
		chemin = (glob.glob(plot_file + '/' + pattern[j]))
		mean = []
		mean = np.loadtxt(str(chemin[0]))
		data.append(mean)
	return data



def plotting(N, data, num_max, label, norma, with_pvalue, with_points, save_data, title, ANOVA, save_dir, eps):
	'''
args : N : (int) numbers of bar in the plot, num_max : (int) number of maximums projected, label : (string) list of names displayed under the bars, norma : (int) the number of the condition which is used for normalised the datas begin at 1, 
with_pvalue : (int) display the p-value on the plot if set to 1, with_points : (int) display the points inside the bars if is set to 1, save_data : (int) save all the datas in .txt files if set to 1, 
title : (string) title of the plot, ANOVA : (int) display p-value of ANOVA test if set to 1, save_dir : (string) way to the directory where datas are saving, eps : (int) saves the plot in .eps
	'''
	means = []
	std = []
	plot_labels = []
	for i in range(N):
		means.append((np.mean(data[i]))/(np.mean(data[norma])))
		std.append((np.std(data[i]))/(np.mean(data[norma])))
		plot_labels.append(label[i] + '(N = ' + str(len(data[i])) + ')')

	ind = np.arange(N)  # the x locations for the groups
	width = 0.2     # the width of the bars
	fig, ax = plt.subplots()
	rects1 = ax.bar(ind, means, width, color='y', yerr=std)
	ax.set_xticks(ind + width / 2)
	ax.set_xticklabels(plot_labels)
	plt.ylim(ymax=3)

	if with_pvalue == 1:
		def label_diff(i,j,text,X,Y):
			'''This function draws a line between two bars of the bar chart and displays a text on top of it.It takes in arguments the i-th and the j-th bar of the bar chart, the text to display, the center of the bars as an array and the y value of the bars as an array.'''
			x = (X[i]+X[j])/2
			y = 1.1*max(Y[i], Y[j])
			dx = abs(X[i]-X[j])
			props = {'arrowstyle':'|-|',\
		                 'lw':2}
			ax.annotate('', xy=(X[i],y+0.2*(i+j)), xytext=(X[j],y+0.2*(i+j)), arrowprops=props)
			ax.annotate(text, xy=(X[i]+dx/2,y+0.2*(i+j)+0.05), zorder=10)

		for i in range(N):
			for j in range(N):
				if j > 0 and i < j :
					st = np.round(sc.stats.mannwhitneyu(data[i],data[j]),6)
					stat = ('p = ' + str(st[1]))
					label_diff(i,j,stat,ind+width*.5,means)
	if with_points == 1:
		for i in range(N):
			plt.plot(np.linspace(i+0.01,i+width-0.01,len(data[i])),data[i]/(np.mean(data[norma])),'ob')

	plt.title(title)

	if ANOVA == 1:
		anova = sc.stats.f_oneway(*data)[1]
		kruskal = sc.stats.kruskal(*data)[1]
		ax.text(0.1, 0.9,'ANOVA : p = ' + str(np.round(anova,10)) +  '\n Kruskal-Wallis test p = ' + str(np.round(kruskal,6)), horizontalalignment='center', verticalalignment='center', transform = ax.transAxes)

	if save_data == 1:
		saving_data(save_dir,label,data)
	
	if eps == 1:
		fig.savefig(str(save_dir) + '/' + title + '.eps', format='eps', dpi=1000)
	plt.show()



