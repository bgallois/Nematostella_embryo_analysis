__author__ = "Benjamin GALLOIS"
__license__ = "GPL"
__version__ = "0.0.0"
__maintainer__ = "Benjamin GALLOIS"
__email__ = "benjamin.gallois@curie.fr"
__status__ = "Production"



import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import skimage
from skimage import io
from skimage.io.collection import ImageCollection,MultiImage
import glob
from skimage import filters as filters


def stack_importation(chemin):
	'''This function imports a stack of images .TIF in a 3D matrix, it takes in arguments the path of the folder.'''
	img = MultiImage(chemin)
	size = len(img)
	shape = img[0].shape
	stack = np.zeros((shape[0],shape[1],size))
	for i in range(size):
		stack[:,:,i] = img[i]
	return stack, size, shape



def number_MIP(matrix,shape,number):
	'''This function makes a N-maximums intensity projection of a 3D matrix, it takes in arguments the 3D matrix, the shape of the 3D matri in a tuple and the number of maximas to project.'''
	projection = np.zeros(shape)
	for x in range(shape[0]):
		for y in range(shape[1]):
			array_sort = np.sort(matrix[x, y, :])
			projection[x, y] = np.sum(array_sort[-number::])
	return projection



def otsufilter(img):
	'''This function applies an Otsu filter, it takes in arguments a matrix and return a mask where the background is set to 0 and the foreground to 1.'''
	val = filters.threshold_otsu(img)
	mask = img < val 
	mask = np.invert(mask)
	return mask



def main(chemin,num_max):
	'''It's the main function of the program, it takes the path to a folder where the stacks .TIFF are placed, it projects the N-maximums, sets the background to 0 and computes the mean intensity normalized by the numbers of pixels non sets to 0.
It takes in argument the path of a folder and return the mean intensity.'''
	(img, size, shape) = stack_importation(chemin)
	projection_corrected = number_MIP(img,shape,num_max)
	mask = otsufilter(projection_corrected)
	plt.imsave("projected_"+str(chemin), mask*projection_corrected)


chemin = (glob.glob("*.TIF"))

for i in range(len(chemin)):
    main(chemin[i],10)
                