__author__ = "Benjamin GALLOIS"
__license__ = "GPL"
__version__ = "2.0.0"
__maintainer__ = "Benjamin GALLOIS"
__email__ = "benjamin.gallois@curie.fr"
__status__ = "Production"




import Tkinter as Tkinter
import tkFileDialog, Tkconstants 
import ttk
from functions import *

class prog():


    def Nematostella_analysis(self, rep, gain, pattern, num_cond, num_max, label, normal, with_pvalue, with_points, save_data, title, ANOVA,save_dir,eps):
        if rep != '':
            data = []
            for j in range(num_cond):
                chemin = (glob.glob(rep + '/' + pattern[j]))
                mean = []
                for i in range(len(chemin)):
                    mean.append((main(chemin[i],num_max)*float(gain[j]))/float(gain[0]))
                data.append(mean)
            plotting(num_cond, data, num_max, label, normal - 1, with_pvalue, with_points, save_data, title, ANOVA,save_dir,eps)




    def Nematostella_plotting(self, plot_files, gain, pattern, num_cond, num_max, label, normal, with_pvalue, with_points, save_data, title, ANOVA,save_dir,eps):
        if plot_file != '':
            data = data_analysis(plot_file, pattern, num_cond)
            save_data = 0
            plotting(num_cond, data, num_max, label, normal - 1, with_pvalue, with_points, save_data, title, ANOVA,save_dir,eps)







class win1():

    def __init__(self, master, root):
        self.master = master
        self.frame = Tkinter.Frame(master)
        global rep
        global save_dir
        rep = ''
        save_dir =''
        self.button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}
        self.tex2 = Tkinter.Label(master, text='Processing of a stack of images and plotting of the result : ')
        self.tex2.pack()
        self.dir = Tkinter.Button(master, text='Select directory', command = self.directory)
        self.dir.pack(**self.button_opt)
        self.tex1 = Tkinter.Label(master, text='Number of maximas to project : ')
        self.tex1.pack()
        self.s = Tkinter.Spinbox(master, from_=1, to=15)
        self.s.pack()
        self.tex4 = Tkinter.Label(master, text='Number of conditions to plot : ')
        self.tex4.pack()
        self.s1 = Tkinter.Spinbox(master, from_=2, to=10)
        self.s1.pack()
        self.tex5 = Tkinter.Label(master, text='Patterns in name file (insert the N patterns separed with ,) : ')
        self.tex5.pack()
        self.pat = Tkinter.Entry(master)
        self.pat.pack(**self.button_opt)
        self.pat.insert(0, "*CtrlMO_*491.TIF,*StbmMO_*491.TIF,*CtrlMO+*491.TIF,*StbmMO+*491.TIF")
        self.tex7 = Tkinter.Label(master, text='Gain : ')
        self.tex7.pack()
        self.gai = Tkinter.Entry(master)
        self.gai.pack(**self.button_opt)
        self.gai.insert(0, "80,50,50,50")
        self.tex7 = Tkinter.Label(master, text='Normalized by (insert the number) : ')
        self.tex7.pack()
        self.norma = Tkinter.Spinbox(master, from_=1, to=10)
        self.norma.pack()
        self.tex6 = Tkinter.Label(master, text='Labels : ')
        self.tex6.pack()
        self.lab = Tkinter.Entry(master)
        self.lab.pack(**self.button_opt)
        self.lab.insert(0, "CtrlMO,StbmMO,CtrlMO rescue,StbmMO rescue")
        self.tex4 = Tkinter.Label(master, text='Options available for the plot')
        self.tex4.pack()
        self.var = Tkinter.IntVar()
        self.check = Tkinter.Checkbutton(master, text="with points", variable = self.var)
        self.check.pack()
        self.var1 = Tkinter.IntVar()
        self.check1 = Tkinter.Checkbutton(master, text="with p-value", variable = self.var1)
        self.check1.pack()
        self.var3 = Tkinter.IntVar()
        self.check3 = Tkinter.Checkbutton(master, text="Perform ANOVA and Kruskal-Wallis test", variable = self.var3)
        self.check3.pack()
        self.var2 = Tkinter.IntVar()
        self.check2 = Tkinter.Checkbutton(master, text="save datas", variable = self.var2)
        self.check2.pack()
        self.var4 = Tkinter.IntVar()
        self.check4 = Tkinter.Checkbutton(master, text="save plot in .eps", variable = self.var4)
        self.check4.pack()
        self.save_dir = Tkinter.Button(master, text='Select directory', command = self.save_directory)
        self.save_dir.pack(**self.button_opt)
        self.tex8 = Tkinter.Label(master, text='Title of the plot : ')
        self.tex8.pack()
        self.tit = Tkinter.Entry(master)
        self.tit.pack(**self.button_opt)
        self.tit.insert(0, " ")
        self.app = Tkinter.Button(master, text='Apply', command = self.get)
        self.app.pack()
        self.btnquitter = Tkinter.Button(master, text='Run the analysis', command = self.run)
        self.btnquitter.pack()


        

    def directory(self): 
        global rep
        dirtext ='Select directory'
        rep = tkFileDialog.askdirectory() 
        self.dir["text"] = str(rep) if rep else dirtext 

    def save_directory(self): 
        global save_dir
        dirtext ='Select directory'
        save_dir = tkFileDialog.askdirectory() 
        self.save_dir["text"] = str(save_dir) if save_dir else dirtext 

    def get(self):
        global num_max
        global num_cond
        global with_points
        global with_pvalue
        global save_data
        global label
        global pattern
        global normal
        global gain
        global title
        global ANOVA
        global eps
        num_max = self.s.get()
        num_max = int(num_max)
        normal = int(self.norma.get())
        num_cond = int(self.s1.get())
        eps = int(self.var4.get())
        with_points = self.var.get()
        with_pvalue = self.var1.get()
        pattern = self.pat.get()
        pattern = pattern.split(',')
        gain = self.gai.get()
        gain = gain.split(',')
        label = self.lab.get()
        label = label.split(',')
        save_data = self.var2.get()
        title = self.tit.get()
        ANOVA = self.var3.get()

 


    def run(self):
        if rep != '':
            prog().Nematostella_analysis(rep, gain, pattern, num_cond, num_max, label, normal, with_pvalue, with_points, save_data, title, ANOVA,save_dir,eps)


class help(Tkinter.Toplevel):

    def __init__(self):
        
        self.title('Help')
        self.geometry('600x600')
        text = Tkinter.Label(self, text='First select the directory where the files are placed.\n Second choose the number of maximas that the program will project for the maximum intensity projection.\n Third select if you want the bar chart with the individual points inside the bar \n and if you want the p-value displayed on top of the bars. \n Contact : benjamin.gallois@curie.fr')
        text.pack()





class Nema_plotting:

    def __init__(self,master, root):
        self.master = master
        self.frame = Tkinter.Frame(master)
        self.button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}
        global plot_file
        global save_dir
        plot_file = ''
        save_dir = ''
        self.tex3 = Tkinter.Label(master, text='Plotting of data already process in a .txt file : ')
        self.tex3.pack()
        self.file = Tkinter.Button(master, text='Select a file', command = self.open_file)
        self.file.pack(**self.button_opt)
        self.tex4 = Tkinter.Label(master, text='Number of conditions to plot : ')
        self.tex4.pack()
        self.s1 = Tkinter.Spinbox(master, from_=2, to=10)
        self.s1.pack()
        self.tex4 = Tkinter.Label(master, text='Options available for the plot')
        self.tex4.pack()
        self.tex7 = Tkinter.Label(master, text='Normalized by (insert the number) : ')
        self.tex7.pack()
        self.norma = Tkinter.Spinbox(master, from_=1, to=10)
        self.norma.pack()
        self.tex5 = Tkinter.Label(master, text='Patterns in name file (insert the N patterns separed with ,) : ')
        self.tex5.pack()
        self.pat = Tkinter.Entry(master)
        self.pat.pack(**self.button_opt)
        self.pat.insert(0, "*CtrlMO.txt,*StbmMO.txt,*CtrlMO res*,*StbmMO res*")
        self.tex6 = Tkinter.Label(master, text='Labels : ')
        self.tex6.pack()
        self.lab = Tkinter.Entry(master)
        self.lab.pack(**self.button_opt)
        self.lab.insert(0, "CtrlMO,StbmMO,CtrlMO rescue,StbmMO rescue")
        self.var = Tkinter.IntVar()
        self.check = Tkinter.Checkbutton(master, text="with points", variable = self.var)
        self.check.pack()
        self.var1 = Tkinter.IntVar()
        self.check1 = Tkinter.Checkbutton(master, text="with p-value", variable = self.var1)
        self.check1.pack()
        self.var3 = Tkinter.IntVar()
        self.check3 = Tkinter.Checkbutton(master, text="Perform ANOVA and Kruskal-Wallis test", variable = self.var3)
        self.check3.pack()
        self.var2 = Tkinter.IntVar()
        self.tex8 = Tkinter.Label(master, text='Title of the plot : ')
        self.tex8.pack()
        self.tit = Tkinter.Entry(master)
        self.tit.pack(**self.button_opt)
        self.tit.insert(0, " ")
        self.var4 = Tkinter.IntVar()
        self.check4 = Tkinter.Checkbutton(master, text="save plot in .eps", variable = self.var4)
        self.check4.pack()
        self.save_dir = Tkinter.Button(master, text='Select directory', command = self.save_directory)
        self.save_dir.pack(**self.button_opt)
        self.app = Tkinter.Button(master, text='Apply', command = self.get)
        self.app.pack()
        self.btnquitter = Tkinter.Button(master, text='Run the analysis', command = self.run)
        self.btnquitter.pack()

    def open_file(self):
        global plot_file
        filetext ='Select a folder'
        plot_file = tkFileDialog.askdirectory() 
        self.file["text"] = str(plot_file) if plot_file else dirtext

    def get(self):
        global num_max
        global num_cond
        global with_points
        global with_pvalue
        global save_data
        global label
        global pattern
        global normal
        global gain
        global title
        global ANOVA
        global eps
        num_max = 50
        normal = int(self.norma.get())
        num_cond = int(self.s1.get())
        with_points = self.var.get()
        with_pvalue = self.var1.get()
        eps = int(self.var4.get())
        pattern = self.pat.get()
        pattern = pattern.split(',')
        gain = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        label = self.lab.get()
        label = label.split(',')
        save_data = self.var2.get()
        title = self.tit.get()
        ANOVA = self.var3.get()
    
    def save_directory(self): 
        global save_dir
        dirtext ='Select directory'
        save_dir = tkFileDialog.askdirectory() 
        self.save_dir["text"] = str(save_dir) if save_dir else dirtext 

    def run(self):
        if plot_file != '':
            prog().Nematostella_plotting(plot_file, gain, pattern, num_cond, num_max, label, normal, with_pvalue, with_points, save_data, title, ANOVA,save_dir,eps)
        



class Nemas_stat:

    def __init__(self,master, root):
        self.master = master
        self.frame = Tkinter.Frame(master)
        self.button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}
        self.file = Tkinter.Button(master, text='Select a file', command = self.open_file)
        self.file.pack(**self.button_opt)
        self.tex1 = Tkinter.Label(master, text='Patterns in name file (insert the N patterns separed with ,) : ')
        self.tex1.pack()
        self.pat = Tkinter.Entry(master)
        self.pat.pack(**self.button_opt)
        self.pat.insert(0, "*CtrlMO.txt,*StbmMO.txt,*CtrlMO res*,*StbmMO res*")
        self.tex2 = Tkinter.label(master, text='Mann Whitney')


root = Tkinter.Tk()
root.title('Nematostella Analysis')
root.geometry('450x700')
root.style = ttk.Style()
#('clam', 'alt', 'default', 'classic')
root.style.theme_use("clam")
notebook=ttk.Notebook(root,width=400, height=650)
frame1=ttk.Frame(notebook)
gui = win1(frame1, root)
frame2=ttk.Frame(notebook)
gui2 = Nema_plotting(frame2, root)
notebook.add(frame1,text="Nematostella Analysis")
notebook.add(frame2,text="Nematostella plotting")
notebook.pack()
root.mainloop()
